# Desafio QA

## Objetivo
A proposta deste desafio é analisar as suas habilidades em conceber cenários de testes, design, programação e boas práticas necessárias para automatização dos testes.
Vamos considerar e avaliar todas etapas, caso tenha dúvidas referentes ao Desafio, sinta-se a vontade para entrar em contato com nosso time.

### 1. Etapa (Frontend)

Nessa etapa é necessário elaborar um relatório contendo os principais problemas encontrados. É importante que o relatório seja organizado e de fácil compreensão.

* Conferir layout esperado no [Figma](https://www.figma.com/file/aSgLsRxNxQHzUQLH6yP8nR/Teste-QA?node-id=0%3A1).
* Conferir projeto desenvolvido em `desafio-frontend`


### 2. Etapa (Backend)

Você deve criar uma suite de testes (automatizada ou não) com a [API](https://www.themoviedb.org/)

* Foque em cenários críticos.
* Em seu README, detalhar como realizar as configurações necessárias para rodar o projeto em nossa máquina local.


### 3. Processo de submissão e prazo de entrega

1. É necessário ter um repositório no GitLab/GitHub para hospedar o projeto.
2. O repositório deverá ser público.
3. Prazo de entrega de 5 dias úteis.
4. Encaminhar o link do repositório e os documentos do teste técnico por email (desafiosti@suno.com.br).


### 4. Considerações finais

Para realiazação dos testes, você pode escolher a linguagem de programação, framework, etc. que você se sentir mais confortável.
Não se esqueça de prover informações detalhadas de como instalar e rodar as suítes de teste.
Vamos considerar e avaliar todas etapas.
